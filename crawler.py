from html.parser import HTMLParser
from urllib.request import urlopen
from urllib import parse
from urllib.parse import urlparse

class LinkParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for (key, value) in attrs:
                if key == 'href':
                    newUrl = parse.urljoin(self.baseUrl, value)
                    self.links = self.links + [newUrl]

    def getLinks(self, url):
        self.links = []
        self.baseUrl = url
        response = urlopen(url)
        if response.getheader('Content-Type')=='text/html':
            htmlBytes = response.read()
            htmlString = htmlBytes.decode("utf-8")
            self.feed(htmlString)
            return htmlString, self.links
        else:
            return "",[]

def spider(url, maxPages):
    pagesToVisit = [url]
    numberVisited = 0
    parsedUrls = []

    startUrl = urlparse(url)    
    while numberVisited < maxPages and pagesToVisit != []:
        numberVisited = numberVisited +1
        url = pagesToVisit[0]
        pagesToVisit = pagesToVisit[1:]
        try:
            parser = LinkParser()
            data, links = parser.getLinks(url)
            print ("Add url to list: ", url)
            parsedDomain = urlparse(url)
            if startUrl.netloc == parsedDomain.netloc:
                parsedUrls.append(url)
            pagesToVisit = pagesToVisit + links
        except:
            print(" **Failed!**")
    print ("Urls parsed", parsedUrls)

def main():
    spider("http://www.dvwa.co.uk/",10)

if __name__ == "__main__":
    main()